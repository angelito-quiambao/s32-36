const express = require('express');
const router = express.Router();

//import units of functions from userController module

const {
    register,
    getAllUsers,
    checkEmail,
    login,
    profile, 
    update,
    updatePassword,
    updateAdmin,
    updateUser,
    deleteUser
} = require('./../controllers/userControllers')

const {
    verify,
    decode,
    verifyAdmin
} = require('./../auth');
// const { update } = require('../models/User');

//GET ALL USERS
router.get('/', async (req, res) =>{

    try{
       await getAllUsers().then(result => res.send (result))
    }
    catch(err){
        res.status.apply(500).json(err)
    }
})

//REGISTER A USER
router.post('/register', async (req, res) =>{
    // console.log(req.body) //user object

    try{
        await register(req.body).then(result => res.send (result))
    }
    catch(err){
        res.status.apply(500).json(err)
    }
})

//CHECK IF EMAIL ALREADY EXIST
router.post('/email-exists', async (req, res) =>{
    try{
       await checkEmail(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status.apply(500).json(err)
    }
})


//LOGIN THE USER
    //authentication
router.post('/login', async (req, res) => {
    // console.log(req.body)
    try{
       await login(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    } 
})


//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
    // res.send(`welcome to get request`)
    const userID = decode(req.headers.authorization).id
    // console.log(userID)

    try{
        await profile(userID).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})


//CREATE A ROUTE TO UPDATE USER INFORMATION, MAKE SURE THE ROUTE IS SECURED WITH TOKEN
// router.put('/:id/update')
router.put('/update', verify, async (req,res) => {
    // console.log(decode(req.headers.authorization).id)
    const userId = decode(req.headers.authorization).id
    try{
        await update(userId, req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//CREATE A ROUTE /user-password TO UPDATE USER'S PASSWORD, MAKE SURE THE ROUTE IS SECURED WITH TOKEN
router.patch('/update-password', verify, async (req,res) =>{

    const userId = decode(req.headers.authorization).id

    try {
        await updatePassword(userId, req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Create a route /isAdmin to update user's isAdmin status to true, make sure the  route is secured with token
	//Only admin can update user's isAdmin status

// router.patch('/isAdmin', verify, async (req,res) =>{
//     // console.log(req.body)
//     // console.log(decode(req.headers.authorization))
//     const admin = decode(req.headers.authorization).isAdmin

//     try{
//         if(admin){
//             await updateStatus(req.body).then(result => res.send(result))
//             // res.send(`You are authorized!`)
//         }
//         else{
//             res.send(`You are not authorized!`)
//         }
//     }
//     catch(err){
//         res.status(500).json(err)   
//     }
// })

// or

router.patch('/isAdmin', verifyAdmin, async (req,res) =>{

    try{
        await updateAdmin(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)   
    }
})

//Create a route /isUser to update user's isAdmin status to false, make sure the  route is secured with token
	//Only admin can update user's isAdmin status
router.patch('/isUser', verifyAdmin, async (req,res) =>{

    try{
        await updateUser(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)   
    }
})

//CREATE A ROUTE /delete TO DELETE A USER FROM THE DB AND RETURN TRUE IF SUCCESSFULL, MAKE SURE THE ROUTE IS SECURED
    //ONLY ADMIN CAN DELETE A USER
router.delete('/delete-user', verifyAdmin, async (req,res) =>{

    try{
        await deleteUser(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)   
    }
})


//Export the router module to be used in index.js file
module.exports = router;