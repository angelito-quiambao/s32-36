//encryption
const CryptoJS = require("crypto-js");
//const bcrypt = require('bcrypt');

//import auth.js
const { createToken } = require('./../auth');

const User = require ('../models/User');
const { findOneAndUpdate } = require("../models/User");


//REGISTER A USER
module.exports.register = async (reqBody) =>{
    // console.log(reqBody)

const {firstName, lastName, email, password} = reqBody

   const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
        //password:bcrypt.hashSync(password, 10)
    })

    return await newUser.save().then(result => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
        }
    })
}

//GET ALL USERS
module.exports.getAllUsers = async () =>{
    return await User.find().then( result => result)
}

//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) =>{
    const {email} = reqBody

    return await User.findOne({email: email}).then((result, err) => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
            else{
                return err
            }
        }
    })
}

//LOGIN A USER

module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				//console.log(reqBody.password == decryptedPw) //true
				
				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}

//RETRIEVE USER INFORMATION
module.exports.profile = async (id) =>{

    return await User.findById(id).then((result, err) => {
        if(result){
            return result
        }
        else{
            if (result == null){
                return {message:`user does not exist`}
            }
            else{
                return err
            }
        }
    })
}

//CREATE A ROUTE TO UPDATE USER INFORMATION, MAKE SURE THE ROUTE IS SECURED WITH TOKEN
module.exports.update = async (userId, reqBody) =>{
    const userData = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }
    
    // console.log(userId)
    return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, err) => {
        if(result){
            result.password = "***"
            return result
        }
        else{
            return err
        }
    })
}

//CREATE A ROUTE /user-password TO UPDATE USER'S PASSWORD, MAKE SURE THE ROUTE IS SECURED WITH TOKEN
module.exports.updatePassword = async (userId, reqBody) => {
    // console.log(userId, reqBody)

    let newPass = { 
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }

    // console.log(newPass)

    return await User.findByIdAndUpdate(userId, {$set: newPass}).then((result, err) => {
        // console.log(result)
        if(result){
            result.password = "***"
            return result
        }
        else{
            return err
        }
    })
}

//Create a route /isAdmin to update user's isAdmin status to true, make sure the  route is secured with token
	//Only admin can update user's isAdmin status
module.exports.updateAdmin = async (reqBody) =>{
    const {email} = reqBody

    return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}).then((result, err) => result ? true : err)
}

//Create a route /isUser to update user's isAdmin status to false, make sure the  route is secured with token
	//Only admin can update user's isAdmin status
module.exports.updateUser = async (reqBody) =>{
    const {email} = reqBody

    return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
}

//CREATE A ROUTE /delete TO DELETE A USER FROM THE DB AND RETURN TRUE IF SUCCESSFULL, MAKE SURE THE ROUTE IS SECURED
    //ONLY ADMIN CAN DELETE A USER

module.exports.deleteUser = async (reqBody) =>{
    const {email} = reqBody

    return await User.findOneAndDelete({email: email}).then((result, err) => result ? true : err)
}    