const Course = require ('../models/Course');

//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course
module.exports.createCourse = async (reqBody) => {

    const {courseName, description, price} = reqBody

    const newCourse = new Course({
        courseName: courseName,
        description: description,
        price: price
    })

    return await newCourse.save().then(result => {
        if(result){
            //  console.log(result)
            return result
        }
        else{
            if (result == null){
                return false
            }
        }
    })
}


//Create a route "/" to get all the courses, return all documents found
	//both admin and regular user can access this route
module.exports.viewCourses = async () => {
    return await Course.find().then( result => result)
}


//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
	//both admin and regular user can access this route
module.exports.viewOneCourse = async (id) =>{
    return await Course.findById(id).then(result =>{
        if(result){
            return result
        }
        else{
            return {message: `Course not existing`}
        }
    })
}


//Create a route "/:courseId/update" to update course info, return the updated document
	//Only admin can update a course
module.exports.courseUpdate = async (id, reqBody) =>{
    // console.log(id, reqBody)
    return await Course.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then((result, err) =>{
        if(result){
            return result
        }
        else{
            return err
        }

    })
}


//Create a route "/:courseId/archive" to update isActive to false, return updated document
	//Only admin can update a course
module.exports.archiveCourse = async (id) =>{
    // console.log(id)
    return await Course.findByIdAndUpdate(id, {$set:{isOffered: false}}, {new: true}).then((result, err) =>{
        if(result){
            return result
        }
        else{
            return err
        }

    })
}


//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
	//Only admin can update a course
module.exports.unarchiveCourse = async (id) =>{
    // console.log(id)
    return await Course.findByIdAndUpdate(id, {$set:{isOffered: true}}, {new: true}).then((result, err) =>{
        if(result){
            return result
        }
        else{
            return err
        }

    })
}

//Create a route "/isActive" to get all active courses, return all documents found
	//both admin and regular user can access this route
module.exports.activeCourses = async () =>{
    return await Course.find({isOffered: true}).then(result => result)
}

//Create a route "/:id/delete-course" to delete a courses, return true if success
	//Only admin can delete a course

module.exports.deleteCourse = async (id) =>{
    // console.log(id)
    return await Course.findByIdAndDelete({_id: id}).then((result, err) => {
        console.log(result)
        if(result){
            return result
        }
        else{
            return err
        }
    })
}


